### theScore

The score take home assignment

## project structure

This project contains two folders, backend and frontend. Both of the projects contain their respective readme's which explain the steps to start the repo, tech stack and some of the decisions I have take. :)

Note - the reason everything is in one commit is because I started two separate git repos for frontend and backend, then realized at the end that I had to submit under one repo. :(
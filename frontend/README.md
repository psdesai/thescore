# Frontend

The front end of this app is basically a simple react app. I haven't dabbled with too much front end in the past two years, so the code might not be most idiomatic. I used the create-react-app to get started.

## Run app

Run the following two commands in this folder will spin up the app in development mode.

- `npm i`
- `npm run start`

The app will be available on [http://localhost:3000](http://localhost:3000). Paste the url in the browser to view it.

### External Libraries used

I used the following external libraries to make my life a bit easier:

- "axios": "^0.23.0",
- "date-fns": "^2.25.0",
- "js-file-download": "^0.4.12",
- "lodash.debounce": "^4.0.8",

**Axios**: A modern js library to simply making xhr requests to the server.

**date-fns**: A light date utility library. Moment is the industry standart, but we only really needed one function to format the date, which is used to set the filename. This is so that a unique filename is generated everytime user downloads the file. (A user can technically time the two clicks 0.5 seconds apart to get a collision, but that's a small edge case)

**js-file-download**: A library to download the file on the front end. For some reason, react app refused to open the browser's file download view when we got the data from the `download` endpoint from api.

**lodash.debounce**: We used debounce function to make sure we aren't firing away calls to event handlers (which eventially talk to api) on every small change. This way we throttle the requests despite making sure the app feels snappy.

### Things to improve

Error handling. I'm assuming that the server is always available. If there is an error, I just catch it and `console.log` it, ideally we would want to notify the user that a file download failed, or we couldn't fetch the table data. This could be done via a `toast` to let the user know.

There is also no form of authentication between frontend and backend, ideally we would pass a token of some sorts to api to verify that the request is legitimitate.

The `.env` file ideally would be a `.env.example` file with mocked out values, but we aren't saving a token or anything so for the purpose of this assignment, it's okay IMO.

I don't set the different state viables in the url, this means if the user refreshes, they lose the state they were in. This is a simple fix, and I was running out of time.

The App isn't responsive

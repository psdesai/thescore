"use-strict";
import React from "react";
import styled from "styled-components";

const Styles = styled.div`
  input {
    border-radius: 5px;
    border: 1px solid gray;
    height: 20px;
  }

  input[type="text"] {
    margin-left: 5px;
  }

  .search-button {
    border-radius: 25px;
    background: #009879;
    border: none;
    color: white;
    font-weight: bold;
    width: 80px;
    height: 25px;
  }

  .top-row {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
  }

  .download-button-wrapper {
    margin-left: auto;
    padding-right: 25px;
  }

  .download-button-wrapper > button {
    padding: 10px 15px;
    border-radius: 25px;
    margin-top: 5px;
    background: #009879;
    border: none;
    color: white;
    font-weight: bold;
  }
  .pagination-controls {
    display: contents;
    margin-left: 5px;
  }

  .page-size,
  .page-index {
    margin: 0px 10px;
  }

  .page-size > input {
    width: 30px;
  }
`;

const PlayerFilter = ({ tableInstance, debouncedFetchData }) => {
  return (
    <Styles>
      <div className="top-row">
        Name:
        <input
          type="text"
          placeholder="Search player.."
          onChange={(e) =>
            tableInstance.current.setGlobalFilter(
              debouncedFetchData({ name: e.target.value || "" })
            )
          }
        />{" "}
      </div>
    </Styles>
  );
};

export default PlayerFilter;

"use-strict";

import React, { useImperativeHandle } from "react";
import styled from "styled-components";
import {
  useTable,
  useSortBy,
  usePagination,
  useFilters,
  useGlobalFilter,
} from "react-table";

const Styles = styled.div`
  table {
    border-collapse: collapse;
    margin: 10px 0;
    font-size: 0.9em;
    font-family: sans-serif;
    min-width: 400px;
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);

    tr {
      border-bottom: 1px solid #dddddd;
      :last-child {
        td {
          border-bottom: 2px solid #3f6a62;
        }
      }
    }

    tbody tr:nth-of-type(even) {
      background-color: #f3f3f3;
    }

    th,
    td {
      padding: 12px 15px;

      :last-child {
        border-right: 0;
      }
    }

    th {
      background-color: #3f6a62;
      color: #ffffff;
      text-align: left;
    }
  }

  .pagination-wrapper {
    display: flex;
    flex-direction: row;
    justify-content: centre;
    align-items: center;
    margin-bottom: 50px;
  }

  .pagination-wrapper > button {
    padding: 10px 15px;
    border-radius: 25px;
    margin-top: 5px;
    background: #009879;
    border: none;
    color: white;
    font-weight: bold;
  }

  .download-button-wrapper {
    margin-left: auto;
    padding-right: 25px;
  }

  .download-button-wrapper > button {
    padding: 10px 15px;
    border-radius: 25px;
    margin-top: 5px;
    background: #009879;
    border: none;
    color: white;
    font-weight: bold;
    cursor: pointer;
  }

  .pagination-controls {
    margin: 0px 5px;
  }

  .pagination-page-number > input {
    width: 40px;
    border-radius: 10px;
    border: 1px solid gray;
    margin-right: 20px;
  }

  .pagination-page-size {
    width: 80px;
    border-radius: 5px;
    font-weight: 200;
  }
`;

const Table = React.forwardRef(
  ({ columns, data, fetchData, loading, downloadFile }, ref) => {
    const instance = useTable(
      {
        columns,
        data,
        initialState: { pageIndex: 0, pageSize: 50 },
        manualPagination: true,
        manualFilters: true,
        manualSortBy: true,
        pageCount: -1,
      },
      useFilters,
      useGlobalFilter,
      useSortBy,
      usePagination
    );

    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      prepareRow,
      page,
      canPreviousPage,
      canNextPage,
      gotoPage,
      nextPage,
      previousPage,
      setPageSize,
      state: { pageIndex, pageSize, sortBy },
    } = instance;

    useImperativeHandle(ref, () => instance);

    React.useEffect(() => {
      fetchData({
        pageIndex,
        pageSize,
        sortBy: sortBy,
      });
    }, [fetchData, pageIndex, pageSize, sortBy]);

    return (
      <Styles>
        {loading ? <h3>loading....</h3> : ""}
        <table {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th {...column.getHeaderProps(column.getSortByToggleProps())}>
                    {column.render("Header")}
                    <span>
                      {column.isSorted
                        ? column.isSortedDesc
                          ? " 🔽"
                          : " 🔼"
                        : ""}
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row, _i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        <div className="pagination-wrapper">
          <button
            className="pagination-controls"
            onClick={() => previousPage()}
            disabled={!canPreviousPage}
          >
            {"<"}
          </button>{" "}
          <button
            className="pagination-controls"
            onClick={() => nextPage()}
            disabled={!canNextPage}
          >
            {">"}
          </button>{" "}
          <span>
            Page <strong>{pageIndex + 1}</strong>{" "}
          </span>
          <span className="pagination-page-number">
            | Go to page:{" "}
            <input
              type="number"
              defaultValue={pageIndex + 1}
              onChange={(e) => {
                const page = e.target.value ? Number(e.target.value) - 1 : 0;
                gotoPage(page);
              }}
            />
          </span>{" "}
          <select
            value={pageSize}
            className="pagination-page-size"
            onChange={(e) => {
              setPageSize(Number(e.target.value));
            }}
          >
            {[10, 20, 30, 40, 50].map((pageSize) => (
              <option key={pageSize} value={pageSize}>
                Show {pageSize}
              </option>
            ))}
          </select>
          <div className="download-button-wrapper">
            <button onClick={downloadFile}>Download as CSV</button>
          </div>
        </div>
      </Styles>
    );
  }
);

export default Table;

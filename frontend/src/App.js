import React, { useRef } from "react";
import { debounce } from "lodash";

import PlayerFilter from "./Components/PlayerFilter";
import Table from "./Components/Table";

import { getRushingStats, downloadStatsAsCSV } from "./helpers/requestHelper";
import columns from "./helpers/columns";

function App() {
  const tableInstance = useRef(null);

  const columnMemo = React.useMemo(() => columns, []);

  const [data, setData] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [playerName, setPlayerName] = React.useState("");

  const fetchIdRef = React.useRef(0);

  const getSortOrder = (isDescending) => {
    switch (isDescending) {
      case true:
        return "desc";
      case false:
        return "asc";
      default:
        return null;
    }
  };

  const fetchData = React.useCallback(
    async ({ pageSize, pageIndex, name, sortBy } = {}) => {
      if (name !== undefined) {
        setPlayerName(name);
      }
      if (pageSize === undefined) {
        pageSize = tableInstance.current.state.pageSize;
      }

      if (pageIndex === undefined) {
        pageIndex = tableInstance.current.state.pageIndex;
      }

      if (sortBy === undefined) {
        sortBy = tableInstance.current.state.sortBy;
      }

      const { id: sortField = "", desc: isDescending = undefined } =
        sortBy[0] || {};
      const sortOrder = getSortOrder(isDescending);

      console.log({ pageIndex, pageSize, name, sortBy, sortField, sortOrder });
      const fetchId = ++fetchIdRef.current;
      setLoading(true);

      if (fetchId === fetchIdRef.current) {
        const data = await getRushingStats({
          pageIndex,
          pageSize,
          sortField,
          sortOrder,
          player: playerName,
        });

        setData(data);
        setLoading(false);
      }
    },
    [playerName]
  );

  const downloadFile = async () => {
    const { pageSize, pageIndex, sortBy } = tableInstance.current.state;
    const { id: sortField = "", desc: isDescending = undefined } =
      sortBy[0] || {};

    const sortOrder = getSortOrder(isDescending);

    await downloadStatsAsCSV({
      pageSize,
      pageIndex,
      sortField,
      sortOrder,
      player: playerName,
    });
  };

  const debouncedDownloadFile = debounce(downloadFile, 500);
  const debouncedFetchData = debounce(fetchData, 250);

  return (
    <>
      <PlayerFilter
        tableInstance={tableInstance}
        debouncedFetchData={debouncedFetchData}
      />
      <Table
        columns={columnMemo}
        data={data}
        fetchData={fetchData}
        loading={loading}
        ref={tableInstance}
        downloadFile={debouncedDownloadFile}
      ></Table>
    </>
  );
}

export default App;

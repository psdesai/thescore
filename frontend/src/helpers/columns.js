const columns = [
  {
    Header: "Player",
    accessor: "Player",
    disableSortBy: true,
  },
  {
    Header: "Team",
    accessor: "Team",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Position",
    accessor: "Pos",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Rushing Attempts Per Game Avergae",
    accessor: "Att/G",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Rushing Attempts",
    accessor: "Att",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Total Rushing Yards",
    accessor: "Yds",
    filterable: false,
    canSort: true,
    // sortDirection: "none",
  },
  {
    Header: "Rushing Average Yards Per Attempt",
    accessor: "Avg",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Rushing Yards Per Game",
    accessor: "Yds/G",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Total Rushing Touch Downs",
    accessor: "TD",
    filterable: false,
  },
  {
    Header: "Longest Rush",
    accessor: "Lng",
    filterable: false,
    // sortInverted: true,
  },
  {
    Header: "Rushing First Downs",
    accessor: "1st",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Rushing First Down Percentage",
    accessor: "1st%",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Rushing 20+ Yards Each",
    accessor: "20+",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Rushing 40+ Yards Each",
    accessor: "40+",
    disableSortBy: true,
    filterable: false,
  },
  {
    Header: "Rushing Fumbles",
    accessor: "FUM",
    disableSortBy: true,
    filterable: false,
  },
];

export default columns;

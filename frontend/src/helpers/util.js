import lightFormat from "date-fns/lightFormat";
import { FILENAME_FORMAT } from "./constants";

const generateFileName = (format = FILENAME_FORMAT) => {
  return lightFormat(new Date(), format);
};

export { generateFileName };

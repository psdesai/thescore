import axios from "axios";
import FileDownload from "js-file-download";

import { generateFileName } from "./util";

console.log("base_url", process.env);
const req = axios.create({
  baseURL: process.env.REACT_APP_API_ENDPOINT,
});

const buildReqParams = (params) => {
  const reqParams = {};

  if (params.player) {
    reqParams.player = params.player;
  }

  if (params.sortField) {
    reqParams.sort_field = params.sortField;
  }

  if (params.sortOrder) {
    reqParams.sort_order = params.sortOrder;
  }

  if (params.pageIndex === 0 && params.pageSize === 0) {
    return reqParams;
  }

  if (params.pageIndex) {
    reqParams.page_index = params.pageIndex;
  }

  if (params.pageSize) {
    reqParams.page_size = params.pageSize;
  }

  return reqParams;
};

const getRushingStats = async (params) => {
  try {
    const reqParams = buildReqParams(params);
    const res = await req.get("/rushing_stats/detail", {
      params: reqParams,
    });
    const { data } = res.data;

    return data;
  } catch (error) {}
};

const downloadStatsAsCSV = async (params) => {
  try {
    const reqParams = buildReqParams(params);

    const res = await req.get("/rushing_stats/detail/download", {
      params: reqParams,
    });

    const { data } = res;
    const filename = generateFileName();

    FileDownload(data, `${filename}.csv`);
  } catch (error) {
    console.log("error", error);
  }
};

export { getRushingStats, downloadStatsAsCSV };

"use strict";

const process = require("process");
const app = require("./src/app");

const logger = require("./src/logger");
const { loadData } = require("./src/helper/loadData");
const DbClient = require("./src/config/database");

const PORT = process.env.PORT || 3001;

process.on("SIGTERM", () => {
  new DbClient().close();
  process.exit(1);
});

app.listen(PORT, async () => {
  try {
    // load rushing data here once. Ideally, this would be in a seperate db
    await loadData();
    logger.info(`Server runnning on port: ${PORT}`);
  } catch (err) {
    logger.error({ err }, "error starting up the app");
  }
});

# Backend

This is the backend portion of the assignment. To run the app, just run in the root folder of this package, run the following commands and the api will be up and running:

- `npm i`
- `npm run start`

### Tech stack

- `node.js`
- `sqlite`
- `jest`

I have picked sqlite, specifically in memory option just for the ease of use. In production, this would be an external database server that the api would talk to. Since sqlite is compatible with postgresql, all our queries including `create` and `read` would remain the same.

### A few thoughts

- The code has complete integration tests, and test both the end points (including downloading the csv functionality).
- The sql queries are parametrized to safe guard from sql injection attacks. Only place I'm not using them is where I can be sure of what the input it, i.e. it is validated to be a certain value rather than checking if it isn't something.
- There is no authentication here as that wasn't stated in the challenge.
- I currently generate the file on the server, but this could lead to scalability issues, a case could be made to handle filehandling on a different server. This way, if there is a malicious user, the entire app doesn't go down, only the button required for downloading the file won't work.
- The max limit is set to 50 just as a fail safe.
- I didn't get time to package all this in a docker app, but ideally both frontend and backend would be in seperate docker containers and docker compose would bring up the entire app. Right now, we need to run two scripts, one in backend folder, and one in frontend folder.
- I have used bunyan logger which basically streams the logs along with extra detail, and based on the env, handles writing to a log file or these logs could be forwarded to a log ingestion service such Kibana(Elasticsearch)/sumologic/splunk/cloudwatch etc to make app monitoring in prod easy.

const jest = require("jest");

module.exports = class Logger {
  constructor() {
    this.error = jest.fn();
    this.info = jest.fn();
  }

  child() {
    return this;
  }
};

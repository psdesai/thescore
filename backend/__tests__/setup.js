"use strict";

const {
  createTable,
  createIndexes,
  insertData,
} = require("../src/helper/loadData");

const mockData = require("./mocks/data");

const loadData = async () => {
  await createTable();
  await createIndexes();
  await insertData(mockData);
};

module.exports = { loadData };

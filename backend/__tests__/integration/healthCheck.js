"use strict";

const request = require("supertest");
const app = require("../../src/app");

it("/health_check - success", async () => {
  const { body } = await request(app).get("/health_check");
  expect(body).toEqual({
    data: "It works",
  });
});

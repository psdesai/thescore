"use strict";

const request = require("supertest");
const app = require("../../src/app");
const DbClient = require("../../src/config/database");

const { loadData } = require("../setup");
const mockData = require("../mocks/data");
const dbClient = new DbClient();

beforeAll(async () => {
  await loadData();
});

afterAll(async () => {
  await dbClient.close();
});

describe("rushing_stats/detail", () => {
  it("/rushing_stats/detail - success", async () => {
    const { body } = await request(app).get("/rushing_stats/detail");
    expect(body.data.length).toEqual(mockData.length);
  });

  it("rushing_stats/detail?player=<name> - success", async () => {
    const { body } = await request(app).get("/rushing_stats/detail?player=Joe");
    expect(body.data[0]).toEqual(mockData[0]);
  });

  it("rushing_stats/detail?sort_field=xyz - fail", async () => {
    const { body } = await request(app).get(
      "/rushing_stats/detail?sort_field=xyz"
    );
    expect(body).toHaveProperty("error");
  });

  it("rushing_stats/detail?sort_field=TD&sort_order=asc - success", async () => {
    const { body } = await request(app).get(
      "/rushing_stats/detail?sort_field=TD&sort_order=asc"
    );
    expect(body.data[0].Player).toEqual(mockData[0].Player);
  });

  it("rushing_stats/detail?sort_field=TD&sort_order=desc - success", async () => {
    const { body } = await request(app).get(
      "/rushing_stats/detail?sort_field=TD&sort_order=desc"
    );
    expect(body.data[0].Player).toEqual(mockData[2].Player);
  });
});

describe("rushing_stats/detail/download", () => {
  let response;

  beforeEach(async () => {
    response = await request(app)
      .get("/rushing_stats/detail/download")
      .buffer()
      .parse((res, callback) => {
        res.setEncoding("binary");
        res.data = "";
        res.on("data", (chunk) => {
          res.data += chunk;
        });
        res.on("end", () => {
          callback(null, Buffer.from(res.data, "binary"));
        });
      });
  });

  afterEach(() => {});
  it("rushing_stats/detail/download - returns correct csv file", async () => {
    const { body } = response;

    expect(response.statusCode).toEqual(200);

    const data = body.toString("utf-8");
    const csvRows = data.split("\n").length;
    expect(csvRows).toEqual(mockData.length + 1);
  });
});

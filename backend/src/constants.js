"use strict";

const DEFAULT_PAGE_SIZE = 50;
const FILENAME_FORMAT = "yyyymmdd_HHMMSS";

module.exports = {
  DEFAULT_PAGE_SIZE,
  FILENAME_FORMAT,
};

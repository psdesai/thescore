"use strict";

const sqlite3 = require("sqlite3").verbose();
const logger = require("../logger");

let instance = null;

class DBClient {
  constructor() {
    if (!instance) {
      instance = this;
      this.logger = logger.child({ class: "DBClient" });
      this.db = new sqlite3.Database(":memory:");
    }

    return instance;
  }

  all(queryStr, params) {
    return new Promise((resolve, reject) => {
      this.db.all(queryStr, params, (err, result) => {
        if (err) {
          this.logger.error({ err }, "error running sql");
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  close() {
    this.logger.info("closing db connection.");
    this.db.close();
  }
}

module.exports = DBClient;

const errorTypes = {
  invalidParams: "invalidParams",
  default: "default",
};

const getErrorMessage = (type = "") => {
  const errorMap = {
    [errorTypes.invalidParams]: {
      type: errorTypes.invalidToken,
      statusCode: 401,
      message:
        "Invalid Request Params provided. Please make sure that you provide valid params.",
    },
    [errorTypes.default]: {
      type: errorTypes.default,
      statusCode: 500,
      message: "Something went wrong. We are on it. Please try again later",
    },
  };

  return errorMap[type];
};

class ApiError extends Error {
  constructor(type, details = "", params) {
    super(params);

    this.setError(type, details);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ApiError);
    }
  }

  setError(type, details) {
    const errorDetails =
      getErrorMessage(type) || getErrorMessage(errorTypes.default);

    this.type = errorDetails.type;
    this.statusCode = errorDetails.statusCode;
    this.message = errorDetails.message;
    this.date = new Date();

    if (details) {
      this.details = details;
    }
  }
}

module.exports = { ApiError, errorTypes };

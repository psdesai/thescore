"use strict";

const logger = require("../logger");
const DbClient = require("../config/database");
const { generateCSVData } = require("../helper/generateCSV");
const { errorTypes, ApiError } = require("../errors/apiError");
const { keysToTablenameMap, tablenameToKeysMap } = require("../helper/util");

const CONSTANTS = require("../constants");
class RushingStats {
  constructor() {
    this.logger = logger.child({ class: "RushingStatsModel" });
    this.dbClient = new DbClient();
  }

  async getDetails(params) {
    const {
      player = "",
      sort_field: sortField = null,
      sort_order: sortOrder = "asc",
      page_size: pageSize = CONSTANTS.DEFAULT_PAGE_SIZE,
      page_index: pageIndex = 0,
    } = params;

    this.checkParams({ sortField, sortOrder, player });

    const [sql, sqlParams] = this.generateSelectQueryAndValues({
      player,
      sortField,
      sortOrder,
      pageSize,
      pageIndex,
    });

    const data = await this.dbClient.all(sql, sqlParams);

    const responseData = this.formatResponseData(data);
    return responseData;
  }

  async generateCSV(data) {
    const csvData = await generateCSVData(data);
    return csvData;
  }

  generateSelectQueryAndValues({
    player,
    sortField,
    sortOrder = "asc",
    pageIndex,
    pageSize,
  }) {
    let sql = "select * from rushing_stats\n";
    let valueObj = {};

    if (player) {
      sql += `where player_name like $player\n`;
      valueObj["$player"] = player + "%";
    }

    if (sortField === "Lng") {
      sql += `order by longest_rush_number_only ${sortOrder}\n`;
    } else if (sortField) {
      const sortFieldTableName = keysToTablenameMap[sortField];
      sql += `order by ${sortFieldTableName} ${sortOrder}\n`;
    }

    sql += `limit ${pageSize} offset ${pageIndex}`;

    return [sql, valueObj];
  }

  formatResponseData(players = []) {
    if (players.length < 1) {
      return players;
    }

    let returnData = [];
    for (let player of players) {
      let returnObj = {};
      delete player.id;
      delete player.longest_rush_number_only;
      for (const key of Object.keys(player)) {
        returnObj[tablenameToKeysMap[key]] = player[key];
      }
      returnData = [...returnData, returnObj];
    }

    return returnData;
  }

  checkParams({
    sortField,
    sortOrder,
    player = "",
    pageSize = 50,
    pageIndex = 0,
  }) {
    if (sortField) {
      let isValidSortField =
        RushingStats.validSortOpts().validSortFields.includes(sortField);

      let isValidSortOrder =
        RushingStats.validSortOpts().validSortOrders.includes(sortOrder);

      if (!isValidSortField) {
        this.logger.error(
          `Received invalid sort field. SortField: ${sortField}`
        );
        throw new ApiError(
          errorTypes.invalidParams,
          "invalid sort_field provided."
        );
      }

      sortOrder = isValidSortOrder ? sortOrder : "asc";
    }

    if (typeof player !== "string") {
      this.logger.error(`Received invalid player name. Player: ${player}`);
      throw new ApiError(
        errorTypes.invalidParams,
        "Invalid player name found."
      );
    }

    if (typeof pageSize !== "number" || pageSize > 50) {
      logger.error(`Received invalid page size. pageSize: ${pageSize}`);
      throw new ApiError(errorTypes.invalidParams, "Invalid pageSize found.");
    }

    if (typeof pageIndex !== "number") {
      logger.error(`Received invalid page index. pageIndex: ${pageIndex}`);
      throw new ApiError(errorTypes.invalidParams, "Invalid pageIndex found.");
    }
  }

  static validSortOpts() {
    const validSortFields = ["Yds", "Lng", "TD"];
    const validSortOrders = ["asc", "desc"];

    return { validSortFields, validSortOrders };
  }
}

module.exports = RushingStats;

"use strict";

const { Router } = require("express");

const { generateFileName } = require("../helper/generateCSV");
const RushingStats = require("../models/rushingStats");
const router = Router();

router.get("/detail", async (req, res) => {
  try {
    const query = req.query;

    const rushingStats = new RushingStats();
    const data = await rushingStats.getDetails(query);

    res.json({ data }).status(200);
  } catch (error) {
    let { statusCode = 500, message } = error;
    res.status(statusCode).json({ error: message });
  }
});

router.get("/detail/download", async (req, res) => {
  try {
    const query = req.query;

    const rushingStats = new RushingStats();
    const data = await rushingStats.getDetails(query);
    const csv = await rushingStats.generateCSV(data);
    const filename = await generateFileName();

    res.header("Content-Type", "text/csv");
    res.header("Content-Length", Buffer.from(csv).length);
    res.header("Content-Disposition", `attachment; filename=${filename}.csv`);
    res.send(csv);
  } catch (error) {
    let { statusCode = 500, message } = error;
    res.status(statusCode).json({ error: message });
  }
});

module.exports = router;

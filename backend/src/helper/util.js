"use strict";

const objectFlip = (obj = {}) => {
  const returnObj = {};
  for (const key in obj) {
    returnObj[obj[key]] = key;
  }

  return returnObj;
};

const keysToTablenameMap = {
  Player: "player_name",
  Team: "team_name",
  Pos: "position",
  "Att/G": "rushing_attempts_per_game_average",
  Att: "rushing_attempts",
  Yds: "total_rushing_yards",
  Avg: "rushing_average_yards_per_attempt",
  "Yds/G": "rushing_yards_per_game",
  TD: "total_rushing_touchdowns",
  Lng: "longest_rush",
  "1st": "rushing_first_down",
  "1st%": "rushing_first_down_percentage",
  "20+": "rushing_twenty_plus_yards_each",
  "40+": "rushing_forty_plus_yards_each",
  FUM: "rushing_fumbles",
};

const tablenameToKeysMap = objectFlip(keysToTablenameMap);

module.exports = {
  objectFlip,
  keysToTablenameMap,
  tablenameToKeysMap,
};

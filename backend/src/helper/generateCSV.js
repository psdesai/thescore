"use strict";

const { lightFormat } = require("date-fns");
const { parseAsync } = require("json2csv");

const { FILENAME_FORMAT } = require("../constants");
const logger = require("../logger");

logger.child({ class: "generateCSV" });

const generateCSVData = async (data = []) => {
  try {
    const fields = Object.keys(data[0]);
    if (!fields) {
      throw new Error("invalid fields...");
    }

    const csv = await parseAsync(data, { fields });
    return csv;
  } catch (err) {
    logger.error({ err }, "something went wrong parsing csv");
  }
};

const generateFileName = (format = FILENAME_FORMAT) => {
  return lightFormat(new Date(), format);
};

module.exports = { generateCSVData, generateFileName };

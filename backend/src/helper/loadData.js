"use strict";
const DBClient = require("../config/database");
const { tablenameToKeysMap } = require("./util");

const RUSHING_STATS_DATA = require("../rushing.json");

const createTable = async () => {
  const sql = `CREATE TABLE IF NOT EXISTS rushing_stats (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    player_name TEXT(200) NOT NULL,
    team_name TEXT,
    position TEXT(5),
    rushing_attempts_per_game_average REAL,
    rushing_attempts INTEGER,
    total_rushing_yards INTEGER,
    rushing_average_yards_per_attempt REAL,
    rushing_yards_per_game REAL,
    total_rushing_touchdowns INTEGER,
    longest_rush TEXT,
    rushing_first_down INTEGER,
    rushing_first_down_percentage REAL,
    rushing_twenty_plus_yards_each INTEGER,
    rushing_forty_plus_yards_each INTEGER,
    rushing_fumbles INTEGER,
    longest_rush_number_only INTEGER
  );
  `;

  const dbClient = new DBClient();
  await dbClient.all(sql);
};

const createIndexes = async () => {
  const playerNameIdxSql =
    "CREATE INDEX IF NOT EXISTS idx_player_name on rushing_stats(player_name);";
  const totalRushingYardsIdxSql =
    "CREATE INDEX IF NOT EXISTS idx_total_rushing_yards on rushing_stats(total_rushing_yards);";
  const longestRushIdxSql =
    "CREATE INDEX IF NOT EXISTS idx_longest_rush_number_only on rushing_stats(longest_rush_number_only);";
  const totalRushingTouchDownsIdxSql =
    "CREATE INDEX IF NOT EXISTS idx_total_rushing_touchdowns on rushing_stats(total_rushing_touchdowns);";

  const dbClient = new DBClient();
  await dbClient.all(playerNameIdxSql);
  await dbClient.all(totalRushingYardsIdxSql);
  await dbClient.all(longestRushIdxSql);
  await dbClient.all(totalRushingTouchDownsIdxSql);
};

const prepareStatement = (playerStats) => {
  let columns = Object.keys(tablenameToKeysMap).join(", ");
  let insertStatement = `Insert into rushing_stats (${columns}, longest_rush_number_only) Values `;

  let placeholder =
    playerStats
      .map((stat) => {
        // add one for the clean longest rush column
        let columnLength = Object.keys(stat).length;
        return `(${"?, ".repeat(columnLength)}?)`;
      })
      .join(", ") + ";";

  insertStatement += placeholder;

  return insertStatement;
};

const insertValues = (playerStats) => {
  return playerStats.map((stat) => {
    let { Lng: longestRush } = stat;

    if (typeof longestRush === "string") {
      if (longestRush.charAt(longestRush.length - 1).toLowerCase() === "t") {
        longestRush = longestRush.substring(0, longestRush.length - 1);
      }
    }
    return [...Object.values(stat), longestRush];
  });
};

const insertData = async (playerData = null) => {
  let playerStats = playerData || RUSHING_STATS_DATA;
  const insertStatement = prepareStatement(playerStats);
  const values = insertValues(playerStats).flat();

  const dbClient = new DBClient();
  await dbClient.all(insertStatement, values);
};

const loadData = async () => {
  await createTable();
  await createIndexes();
  await insertData();
};

module.exports = { loadData, createTable, createIndexes, insertData };

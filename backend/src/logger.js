"use strict";

const { createLogger, stdSerializers } = require("bunyan");
const PrettyStream = require("bunyan-prettystream");

const pjson = require("../package.json");

const { env, stdout } = process;
const { LOG_LEVEL, NODE_ENV } = env;

const level = LOG_LEVEL || "info";
const nodeEnv = NODE_ENV || "production";

const stream = (function getWritable() {
  if (nodeEnv === "development") {
    const prettyStdOut = new PrettyStream({ mode: "dev" });
    prettyStdOut.pipe(stdout);
    return prettyStdOut;
  }

  return stdout;
})();

module.exports = createLogger({
  name: pjson.name.replace(/^@[a-zA-Z0-9-]+\//g, ""),
  streams: [
    {
      level,
      stream,
    },
  ],
  serializers: stdSerializers,
});

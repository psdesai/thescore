// express stuff
const express = require("express");
const cors = require("cors");

require("dotenv").config();const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.use("/rushing_stats", require("./routes/rushingStats"));

app.get("/health_check", (_req, res) => {
  res.json({ data: "It works" }).status(200);
});

module.exports = app;
